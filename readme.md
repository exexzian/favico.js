#favico.js.
More info [here](http://lab.ejci.net/favico.js/).


Author [Miroslav Magda](http://blog.ejci.net)
Version 0.1.1

##License
All code is open source and dual licensed under GPL and MIT. Check the individual licenses for more information.

###Change log
####0.1.1
* Improving performance in chrome 
####0.1.0
* proof of concept 